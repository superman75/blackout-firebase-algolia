export const environment = {
  production: true,
  version: require('../../package.json').version,
  firebase: {
    apiKey: 'AIzaSyAWG1uQGEegtSgIYqLEBeQDgV1_Ca3YBRE',
    authDomain: 'blackout-lighting-console.firebaseapp.com',
    databaseURL: 'https://blackout-lighting-console.firebaseio.com',
    projectId: 'blackout-lighting-console',
    storageBucket: 'blackout-lighting-console.appspot.com',
    messagingSenderId: '7096933374'
  }
};
