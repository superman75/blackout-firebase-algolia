import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { auth } from 'firebase/app';

import { AuthenticationService } from './service/authentication.service';

@Component({
    templateUrl: './authentication.component.html'
})
export class AuthenticationComponent {
    formGroup: FormGroup;

    constructor(private router: Router, private authenticationService: AuthenticationService, formBuilder: FormBuilder) {
        this.formGroup = formBuilder.group({
            'email': new FormControl('', [
                Validators.required,
                Validators.email
            ]),
            'password': new FormControl('', [
                Validators.required
            ])
        });
    }

    onLoginButtonClick() {
        this.authenticationService.login(this.formGroup.controls['email'].value, this.formGroup.controls['password'].value)
            .subscribe((userCredential: auth.UserCredential) => {
                if (userCredential) {
                    this.router.navigate(['/collection']);
                }
            });
    }
}
