import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { map, take, finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class AuthenticationGuard implements CanActivate {

    constructor(private router: Router, private angularFireAuth: AngularFireAuth, private ngxSpinnerService: NgxSpinnerService) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        this.ngxSpinnerService.show();
        return this.angularFireAuth.authState
            .pipe(
                take(1),
                map((user) => {
                    const loggedIn = user !== null,
                        url = next.url.toString();

                    if (url === 'login') {
                        if (loggedIn) {
                            this.router.navigate(['/collection']);
                        }
                        return !loggedIn;
                    } else {
                        if (!loggedIn) {
                            this.router.navigate(['/login']);
                        }
                        return loggedIn;
                    }
                }),
                finalize(() => this.ngxSpinnerService.hide())
            );
    }
}
