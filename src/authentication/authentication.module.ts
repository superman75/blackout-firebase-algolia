import { NgModule } from '@angular/core';

import { SharedModule } from 'src/shared/shared.module';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AuthenticationComponent } from './authentication.component';

@NgModule({
    declarations: [
        AuthenticationComponent
    ],
    imports: [
        SharedModule,
        AuthenticationRoutingModule
    ]
})
export class AuthenticationModule { }
