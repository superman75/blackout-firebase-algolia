import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { from, of, Observable } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class AuthenticationService {

    constructor(private angularFireAuth: AngularFireAuth, private ngxSpinnerService: NgxSpinnerService) { }

    login(email: string, password: string): Observable<any> {
        this.ngxSpinnerService.show();
        return from(this.angularFireAuth.auth.signInWithEmailAndPassword(email, password))
            .pipe(
                catchError((error) => of(alert(error.message))),
                finalize(() => this.ngxSpinnerService.hide())
            );
    }

    logout() {
        this.ngxSpinnerService.show();
        return from(this.angularFireAuth.auth.signOut())
            .pipe(finalize(() => this.ngxSpinnerService.hide()));
    }
}
