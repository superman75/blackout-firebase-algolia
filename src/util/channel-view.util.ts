export function parseNumber(value: string): number {
    return parseFloat(value);
}

export function isNumber(data: number): boolean {
    return !isNaN(data);
}

export function valueToPercentage(value: number, min: number, max: number) {
    return ((value - min) * 100) / (max - min);
}

export function percentageToValue(value: number, min: number, max: number) {
    return (value * (max - min)) / 100 + min;
}

export function withInRange(value: number, min: number, max: number) {
    if (value > max) {
        value = max;
    } else if (value < min || isNaN(value)) {
        value = min;
    }
    return value;
}
