import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationGuard } from 'src/authentication/guard/authentication.guard';

const routes: Routes = [
  { path: 'login', loadChildren: 'src/authentication/authentication.module#AuthenticationModule', canActivate: [AuthenticationGuard] },
  { path: 'collection', loadChildren: 'src/collection/collection.module#CollectionModule', canActivate: [AuthenticationGuard] },
  { path: '**', redirectTo: '/collection' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
