import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

import { environment } from 'src/environments/environment';
import { ConfigService } from 'src/core/service/config.service';
import { AuthenticationService } from 'src/authentication/service/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.scss'
  ]
})
export class AppComponent {

  version: string = environment.version;

  constructor(private router: Router, public angularFireAut: AngularFireAuth, private authenticationService: AuthenticationService,
    public configService: ConfigService) { }

  onLogoutButtonClick() {
    this.authenticationService.logout()
      .subscribe(() => {
        this.router.navigate(['/login']);
      });
  }
}
