import config from 'src/assets/data/config.json';
import { firestore } from 'firebase/app';

export interface Route {
    path: string;
    collection: string;
    title: string;
}

export interface Collection {
    id: string;
    fields: Field[];
    sort?: { field: string; order: firestore.OrderByDirection };
    descendants?: { collection: string; field: string; }[];
}

export interface Property {
    name: string;
    cellEditor: string;
    value: object;
}

export interface Field {
    id: string;
    name: string;
    type: string;
    reference?: string;
    column: boolean;
    valueGetter?: string;
    cellRenderer?: string;
    required: boolean;
    pattern?: string;
    min?: number;
    max?: number;
    options?: boolean | Option[] | OptionGroup[];
}

export interface Option {
    value: any;
    label: string;
}

export interface OptionGroup {
    title: string;
    items: { display: string, value: any }[];
}

export class ConfigService {
    routes: Route[];
    collections: Collection[];
    properties: Property[];

    constructor() {
        this.routes = config.routes;
        this.collections = config.collections;
        this.properties = config.properties;
    }

    normalizeName(value: string): string {
        return value.split('_').map((str) => `${str[0].toUpperCase()}${str.substr(1)}`).join('_').replace(/_/g, ' ');
    }

    normalizeChannelNumber(value: number): string {
        return `Channel ${value}`;
    }
}
