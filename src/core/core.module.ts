import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { environment } from 'src/environments/environment';
import { AuthenticationGuard } from 'src/authentication/guard/authentication.guard';
import { AuthenticationService } from 'src/authentication/service/authentication.service';

import { ConfigService } from './service/config.service';

@NgModule({
    imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule
    ],
    providers: [
        AuthenticationGuard,
        AuthenticationService,
        ConfigService
    ]
})
export class CoremModule { }
