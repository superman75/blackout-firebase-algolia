import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { ConfigService } from 'src/core/service/config.service';

@Injectable()
export class CollectionGuard implements CanActivate {

    constructor(private router: Router, private configService: ConfigService) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const routes = this.configService.routes,
            path = next.params['path'];

        if (!path || !routes.find((route) => route.path === path)) {
            this.router.navigate(['/collection', routes[0].path]);
            return false;
        }
        return true;
    }
}
