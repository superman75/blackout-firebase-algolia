import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CollectionComponent } from './collection.component';
import { CollectionGuard } from './guard/collection.guard';

const routes: Routes = [
    { path: ':path', component: CollectionComponent, canActivate: [CollectionGuard] },
    { path: '**', canActivate: [CollectionGuard] }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CollectionRoutingModule { }
