import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { OptionGroup } from 'src/core/service/config.service';

interface MatDialogData {
    title: string;
    groups: OptionGroup[];
}

@Component({
    templateUrl: './list-picker.component.html',
    styleUrls: [
        './list-picker.component.scss'
    ]
})
export class ListPickerComponent {

    displayedColumns: string[] = ['display'];

    constructor(private matDialogRef: MatDialogRef<ListPickerComponent>, @Inject(MAT_DIALOG_DATA) public data: MatDialogData) { }

    onRowTap(item: any) {
        this.matDialogRef.close(item.value);
    }
}
