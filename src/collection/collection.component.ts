import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute , Router, NavigationExtras} from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription, of, forkJoin, Observable, from } from 'rxjs';
import { catchError, finalize, switchMap, tap, } from 'rxjs/operators';
import * as moment from 'moment';
import {
    GridApi,
    GridOptions,
    ColDef,
    IGetRowsParams,
    GridReadyEvent,
    ColumnApi,
    ColumnResizedEvent,
    ColumnMovedEvent,
    SelectionChangedEvent,
    RowValueChangedEvent
} from 'ag-grid-community';

import * as algoliasearch from 'algoliasearch/lite';

import { Route, Collection, ConfigService } from 'src/core/service/config.service';
import { FormComponent } from './form/form.component';
import { DescendantCellRendererComponent } from './descendant-cell-renderer/descendant-cell-renderer.component';

const LOADING_TEXT: string = 'loading, please wait...';

const searchClient = algoliasearch(
    'MB2U9S8IR9',
    'a270db16f7aae320b966b5d5ae45963b'
  );

let filter_id: string = '';
let filter_field: string = '';
let publicQueryParams: any;
export interface BreadCrumb {
    type: string;
    text: string;
    path: string;
    queryParams: any;
}

@Component({
    templateUrl: './collection.component.html',
    styleUrls: [
        './collection.component.scss'
    ]
})

export class CollectionComponent implements OnInit, OnDestroy {
    manufactures_config = {
        searchClient,
        indexName: 'manufactures',
        searchFunction(helper) {
            if (helper.hasRefinements('deleted')) {
                helper.search();
                return;
            }
            if (!publicQueryParams['deleted']) {
                helper.addDisjunctiveFacetRefinement('deleted', false);
            } else {
                helper.addDisjunctiveFacetRefinement('deleted', true);
            }
            helper.search();
        }
      };
    fixture_folders_config = {
        searchClient,
        indexName: 'fixture_folders',
        searchFunction(helper) {
            if (helper.hasRefinements('deleted')) {
                helper.search();
                return;
            }
            if (!publicQueryParams['deleted']) {
                helper.addDisjunctiveFacetRefinement('deleted', false);
            } else {
                helper.addDisjunctiveFacetRefinement('deleted', true);
            }
            helper.search();
        }
    };
    fixtures_config = {
        searchClient,
        indexName: 'fixtures',
        searchFunction(helper) {
            console.log(helper.getState());
            if (helper.hasRefinements('deleted')) {
                helper.search();
                return;
            }
            if (!publicQueryParams['deleted']) {
                helper.addDisjunctiveFacetRefinement('deleted', false);
            } else {
                helper.addDisjunctiveFacetRefinement('deleted', true);
            }
            if (filter_field) {
                helper.addHierarchicalFacetRefinement(filter_field + '._path.segments', filter_id);
            }
            helper.search();
        }
    };
    fixture_channels_config = {
        searchClient,
        indexName : 'fixture_channels',
        searchFunction(helper) {
            if (helper.hasRefinements('deleted')) {
                helper.search();
                return;
            }
            if (!publicQueryParams['deleted']) {
                helper.addDisjunctiveFacetRefinement('deleted', false);
            } else {
                helper.addDisjunctiveFacetRefinement('deleted', true);
            }
            helper.addHierarchicalFacetRefinement('fixture_id._path.segments', filter_id);
            helper.search();
        }
    };
    channel_conditions_config = {
        searchClient,
        indexName : 'channel_conditions',
        searchFunction(helper) {
            if (helper.hasRefinements('deleted')) {
                helper.search();
                return;
            }
            if (!publicQueryParams['deleted']) {
                helper.addDisjunctiveFacetRefinement('deleted', false);
            } else {
                helper.addDisjunctiveFacetRefinement('deleted', true);
            }
            helper.addHierarchicalFacetRefinement('channel_id._path.segments', filter_id);
            helper.search();
        }
    };
    channel_functions_config = {
        searchClient,
        indexName: 'channel_functions',
        searchFunction(helper) {
            if (helper.hasRefinements('deleted')) {
                helper.search();
                return;
            }
            if (!publicQueryParams['deleted']) {
                helper.addDisjunctiveFacetRefinement('deleted', false);
            } else {
                helper.addDisjunctiveFacetRefinement('deleted', true);
            }
            helper.addHierarchicalFacetRefinement('channel_id._path.segments', filter_id);
            helper.search();
        }
    };

    rowData: any[] = [];
    gridOptions: GridOptions = {
        rowSelection: 'multiple',
        rowMultiSelectWithClick: true,
        // enableServerSideSorting: true,
        suppressMultiSort: true,
        enableColResize: true,
        enableCellChangeFlash: true,
        // rowModelType: 'infinite',
        cacheBlockSize: 25,
        maxBlocksInCache: 1000,
        maxConcurrentDatasourceRequests: 1,
        infiniteInitialRowCount: 1,
        components: {
            loadingCellRenderer: (params): string => {
                if (params.value !== void 0 && params.value !== null) {
                    return params.value;
                }
                return LOADING_TEXT;
            },
            channelNumberCellRenderer: (params): string => {
                if (params.value !== void 0 && params.value !== null) {
                    return this.configService.normalizeChannelNumber(params.value);
                }
                return LOADING_TEXT;
            }
        },
        frameworkComponents: {
            descendantCellRenderer: DescendantCellRendererComponent
        }
    };
    columnDefs: ColDef[] = [{
        width: 70,
        suppressSizeToFit: true,
        checkboxSelection: true
    }];
    selectedRows: any[] = [];
    copiedRows: any[] = [];
    formGroup: FormGroup;
    bcPriority = {
        manufacture_id: 1,
        folder_id: 2,
        fixture_id: 3,
        channel_id: 4
    };
    editType = 'fullRow';
    bcList: BreadCrumb[];
    collection: Collection;

    private route: Route;
    private gridApi: GridApi;
    private columnApi: ColumnApi;
    private activeRequests: IGetRowsParams[] = [];
    private docs: firestore.DocumentSnapshot[];
    private subscription: Subscription;


    constructor(private activatedRoute: ActivatedRoute, private configService: ConfigService, private matDialog: MatDialog,
        private angularFirestore: AngularFirestore, private ngxSpinnerService: NgxSpinnerService, formBuilder: FormBuilder,
        private router: Router) {
        }

    ngOnInit() {
        this.subscription = this.activatedRoute.params.subscribe(() => {
            this.selectedRows = [];
            const path = this.activatedRoute.snapshot.params['path'];
            this.route = this.configService.routes.find((route) => route.path === path);
            this.collection = this.configService.collections.find((collection) => collection.id === this.route.collection);
            const queryParams = this.activatedRoute.snapshot.queryParams;
            publicQueryParams = queryParams;
            filter_id = '';
            filter_field = '';
            for (const key in queryParams) {
                if (queryParams[key]) {
                    const refField = this.collection.fields.find((field) => field.id === key);
                    if (refField) {
                        filter_id = queryParams[key];
                        filter_field = key;
                    }
                }
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    transformItemsPosition(items) {
        return items.map(item => ({
            ...item,
            label: item.label === 'true' ? 'Head' : 'Cell',
          }));
    }

    transformItemsSnap(items) {
        return items.map(item => ({
            ...item,
            label: item.label === '0' ? 'Slider' : (item.label === '1' ? 'Buttons' : 'Both'),
          }));
    }

    transformItemsApprove(items) {
        return items.map(item => ({
            ...item,
            label: item.label === 'true' ? 'Yes' : 'No',
          }));
    }
    onAddButtonClick() {
        this.openForm({});
    }

    async onEditButtonClick() {
        const activeRef = await this.angularFirestore.collection(this.collection.id).ref.get();
        let doc: any;
        const fbData: any[] = [];
        for (doc of activeRef.docs) {
            if (this.isExistInSelectedRows(doc.id)) {
                fbData.push({id: doc.id, ...doc.data()});
            }
        }
        this.openForm({...fbData});
    }

    private isExistInSelectedRows(key: string) {
        return this.selectedRows.find(row => row.objectID === key) !== undefined;
    }

    onCopyButtonClick() {
        if (confirm('Are you sure, do you want to copy the selected record(s)?')) {
            this.copiedRows = this.selectedRows;
        }
    }

    onPasteButtonClick() {
        if (confirm('Are you sure, do you want to paste the selected record(s)?')) {
            const newIds: { [key: string]: { [key: string]: string } } = {};
            this.processRequest(true, this.copiedRows, (aDoc) =>
                aDoc.get()
                    .pipe(
                        switchMap((documentSnapshot) => {
                            const newId = this.angularFirestore.createId(),
                                collectionId = aDoc.ref.parent.path,
                                currentCollection = this.configService.collections.find((collection) => collection.id === collectionId),
                                activeQueryParams = this.activatedRoute.snapshot.queryParams,
                                data = documentSnapshot.data();

                            if (!newIds[collectionId]) {
                                newIds[collectionId] = {};
                            }
                            newIds[collectionId][documentSnapshot.id] = newId;

                            currentCollection.fields.forEach((field) => {
                                if (field.type === 'reference') {
                                    const reference = field.reference,
                                        newRefId = newIds[reference] && newIds[reference][(<firestore.DocumentReference>data[field.id]).id];

                                    if (newRefId) {
                                        data[field.id] = this.angularFirestore
                                            .collection(reference)
                                            .doc(newRefId).ref;
                                    }
                                }
                            });

                            Object.keys(activeQueryParams).forEach(param => {
                                const collection = currentCollection.fields.find(field => field.id === param);

                                if (collection) {
                                    data[param] = this.angularFirestore
                                        .collection(collection.reference)
                                        .doc(activeQueryParams[param]).ref;
                                }
                            });

                            data['updated_at'] = firestore.FieldValue.serverTimestamp();
                            return from(this.angularFirestore.collection(collectionId).doc(newId).set(data));
                        })
                    )
                , 'before');
        }
    }

    onCloneButtonClick() {
        if (confirm('Are you sure, do you want to clone the selected record(s)?')) {
            const newIds: { [key: string]: { [key: string]: string } } = {};
            this.processRequest(true, this.selectedRows, (aDoc) =>
                aDoc.get()
                    .pipe(
                        switchMap((documentSnapshot) => {
                            const newId = this.angularFirestore.createId(),
                                collectionId = aDoc.ref.parent.path,
                                currentCollection = this.configService.collections.find((collection) => collection.id === collectionId),
                                primaryField = currentCollection.fields.find((field) => field.column),
                                activeQueryParams = this.activatedRoute.snapshot.queryParams,
                                currentId = Object.keys(activeQueryParams).sort().pop(),
                                data = documentSnapshot.data();

                            if (!newIds[collectionId]) {
                                newIds[collectionId] = {};
                            }
                            newIds[collectionId][documentSnapshot.id] = newId;

                            currentCollection.fields.forEach((field) => {
                                if (field.type === 'reference') {
                                    const reference = field.reference,
                                        newRefId = newIds[reference] && newIds[reference][(<firestore.DocumentReference>data[field.id]).id];

                                    if (newRefId) {
                                        data[field.id] = this.angularFirestore
                                            .collection(reference)
                                            .doc(newRefId).ref;
                                    } else if (field.id === currentId && primaryField && primaryField.type === 'string') {
                                        data[primaryField.id] = `${data[primaryField.id]} Copy`;
                                    }
                                }
                            });

                            data['updated_at'] = firestore.FieldValue.serverTimestamp();
                            return from(this.angularFirestore.collection(collectionId).doc(newId).set(data));
                        })
                    )
                , 'before');
        }
    }

    onApproveButtonClick() {
        const fieldValue = {
            'approved': true,
            'updated_at': firestore.FieldValue.serverTimestamp()
        };
        this.processRequest(confirm('Do you want to approve all the descendant record(s) too?'), this.selectedRows,
            (aDoc) => from(aDoc.update(fieldValue)),
            'after');
    }

    onRejectButtonClick() {
        const fieldValue = {
            'approved': false,
            'updated_at': firestore.FieldValue.serverTimestamp()
        };
        this.processRequest(confirm('Do you want to reject all the descendant record(s) too?'), this.selectedRows,
            (aDoc) => from(aDoc.update(fieldValue)),
            'after');
    }

    onDeleteButtonClick() {
        const hasDeleted = !!this.activatedRoute.snapshot.queryParams['deleted'];
        if (confirm('Are you sure, do you want to delete the selected record(s) and it\'s descendant(s)?')) {
            if (hasDeleted) {
                this.processRequest(true, this.selectedRows, (aDoc) => from(aDoc.delete()), 'after');
            } else {
                const fieldValue = {
                    'approved': true,
                    'deleted': true,
                    'updated_at': firestore.FieldValue.serverTimestamp()
                };
                this.processRequest(true, this.selectedRows, (aDoc) => from(aDoc.update(fieldValue)), 'after');
            }
        }
    }

    onGridReady(event: GridReadyEvent) {
        this.gridApi = event.api;
        this.columnApi = event.columnApi;
        this.updateGrid();
    }

    onColumnResized(event: ColumnResizedEvent) {
        if (event.finished) {
            const column = event.column;
            this.setValue(this.getStorageKey(this.collection.id, column.getColDef().headerName), column.getActualWidth());
        }
    }

    onColumnMoved(event: ColumnMovedEvent) {
        const columnState = event.columnApi.getColumnState();
        const activePath = this.activatedRoute.snapshot.params['path'];
        localStorage.setItem(activePath, JSON.stringify(columnState));
    }

    onSelectionChanged(event: SelectionChangedEvent) {
        this.selectedRows = event.api.getSelectedRows();
    }


    private updateGrid() {
        const gridApi = this.gridApi;
        const activeQueryParams = this.activatedRoute.snapshot.queryParams;
        const activePath = this.activatedRoute.snapshot.params['path'];
        const bcTree = Object.keys(activeQueryParams).sort((a, b) => {
            return this.bcPriority[a] - this.bcPriority[b];
        });
        if (bcTree.indexOf('deleted') >= 0) { bcTree.splice(bcTree.indexOf('deleted'), 1); }
        const collectionData = {};
        forkJoin(bcTree.map((type) => {
            const collection = this.configService.collections.find(
                (collect) => (collect.descendants || []).some((d) => d.field === type));
            return this.angularFirestore.collection(collection.id).doc(activeQueryParams[type]).get();
        }))
        .pipe(
            switchMap((documentSnapshots) => {
                return documentSnapshots.map((snapshot, index) => {
                    if (snapshot.exists) {
                        return {
                            'type': bcTree[index],
                            ...snapshot.data()
                        };
                    }
                    return null;
                });
            }),
            tap((item) => {
                collectionData[item['type']] = item;
            }),
            finalize(() => {
                this.bcList = bcTree.map((type, index, upon) => {
                    const queryParams = {};
                    upon.forEach((u, ind) => {
                        if (ind <= index) {
                            queryParams[u] = activeQueryParams[u];
                        }
                    });
                    const collections = this.configService.collections.find(
                        (item) => (item.descendants || []).some((d) => d.field === type));
                    const routes = this.configService.routes.find((item) => item.collection === collections.id);
                    const nextCollection = collections.descendants.length === 1 ? collections.descendants[0].collection : null;
                    let nextPath = null;
                    if (nextCollection) {
                        nextPath = this.configService.routes.find((item) => item.collection === nextCollection);
                    }

                    return {
                        type,
                        text: collectionData[type].name || collectionData[type].description || routes.path,
                        path: nextPath && nextPath.path || null,
                        queryParams
                    };
                });

                if (this.bcList.length > 0) {
                    if (this.bcList[0]['type'] === 'manufacture_id') {
                        this.bcList.splice(0, 0, {
                            type: 'manufactures',
                            text: 'Manufacturer',
                            path: '',
                            queryParams: '',
                        });
                    }
                    if (this.bcList[0]['type'] === 'folder_id') {
                        this.bcList.splice(0, 0, {
                            type: 'folders',
                            text: 'Fixture-Folders',
                            path: 'fixture-folders',
                            queryParams: '',
                        });
                    }
                }
                this.bcList.splice(0, 0, {
                    type: 'home',
                    text: 'Home',
                    path: '',
                    queryParams: '',
                });
                const route = this.configService.routes.find((item) => item.path === activePath);
                const collection = this.configService.collections.find((item) => item.id === route.collection);
                if (!this.configService.collections.some((c) => (c.descendants || []).some((d) => d.collection === route.collection))) {
                    this.bcList.push({
                        type: activePath,
                        text: activePath,
                        path: null,
                        queryParams: '',
                    });
                } else if (!collection.descendants) {
                    this.bcList.push({
                        type: activePath,
                        text: activePath,
                        path: activePath,
                        queryParams: activeQueryParams
                    });
                }
            })
        )
        .subscribe();

        if (gridApi) {
            const path = this.activatedRoute.snapshot.params['path'],
             columnDefs = this.columnDefs;
            columnDefs.splice(1, columnDefs.length);
            this.collection.fields.forEach((field) => {
                if (field.column) {
                    const columnDef: ColDef = {
                        headerName: field.name,
                        field: field.id,
                        valueGetter: field.valueGetter,
                        cellRenderer: field.cellRenderer
                    },
                        width = this.getValue(this.getStorageKey(this.collection.id, columnDef.headerName)),
                        property = this.configService.properties.find((item) => item.name === field.name);

                    if ( field.type === 'string' || field.type === 'number' || field.type === 'boolean'
                    ) {
                        columnDef.editable = true;
                        if (property) {
                            columnDef.cellEditor = property.cellEditor;
                            columnDef.cellEditorParams = { values: this.getKeys(property.value) };
                            columnDef.valueFormatter = (param) => property.value[param.value];
                        }
                    }

                    if (width) {
                        Object.assign(columnDef, {
                            suppressSizeToFit: true,
                            width
                        });
                    }
                    columnDefs.push(columnDef);
                }
            });

            const descendants = this.collection.descendants;
            if (descendants) {
                descendants.forEach((descendant) => {
                    const columnDef = {
                        headerName: this.configService.normalizeName(descendant.collection),
                        cellRenderer: 'descendantCellRenderer',
                        descendant
                    },
                        width = this.getValue(this.getStorageKey(this.collection.id, columnDef.headerName));
                    if (width) {
                        Object.assign(columnDef, {
                            suppressSizeToFit: true,
                            width
                        });
                    }
                    columnDefs.push(columnDef);
                });
            }
            gridApi.setColumnDefs(columnDefs);
            gridApi.sizeColumnsToFit();

            const columnIds = localStorage.getItem(path);
            if (columnIds) {
                this.columnApi.setColumnState(JSON.parse(columnIds));
            }
        }
    }

    private getKeys(items) {
        const keys = Object.keys(items).map(item => {
            if (item === 'true') {
                return true;
            } else if (item === 'false') {
                return false;
            } else {
                return Number(item);
            }
        });
        return keys;
    }

    private processRequest(descendants: boolean, rows: any[], operation: (aDoc: AngularFirestoreDocument) => Observable<any>,
        operate: 'before' | 'after') {
        const selectedRows = rows,
            isAfter = operate === 'after',
            processDescendants = (collectionId, docId): Observable<any> => {
                const decendantItems = this.configService.collections.find((collection) => collection.id === collectionId).descendants;
                if (decendantItems) {
                    return forkJoin(decendantItems.map((descendant) => this.angularFirestore.collection(descendant.collection, (ref) =>
                        ref.where(descendant.field, '==', this.angularFirestore.collection(collectionId).doc(docId).ref))
                        .get()
                        .pipe(
                            switchMap((querySnapshot) => {
                                const docs = querySnapshot.docs;
                                if (docs.length) {
                                    return forkJoin(docs.map((doc) => {
                                        if (isAfter) {
                                            return processDescendants(descendant.collection, doc.id)
                                                .pipe(switchMap(() => operation(this.angularFirestore.doc(doc.ref))));
                                        }
                                        return operation(this.angularFirestore.doc(doc.ref))
                                            .pipe(switchMap(() => processDescendants(descendant.collection, doc.id)));
                                    }));
                                }
                                return of(null);
                            })
                        )));
                }
                return of(null);
            };

        this.ngxSpinnerService.show();
        forkJoin(selectedRows.map((selectedRow) => {
            if (isAfter) {
                return (descendants ? processDescendants(this.collection.id, selectedRow.objectID) : of(null))
                    .pipe(switchMap(() => operation(this.angularFirestore.collection(this.collection.id).doc(selectedRow.objectID))));
            }
            return operation(this.angularFirestore.collection(this.collection.id).doc(selectedRow.objectID))
                .pipe(switchMap(() => (descendants ? processDescendants(this.collection.id, selectedRow.objectID) : of(null))));
        })).pipe(
            catchError((error) => {
                alert(error.message);
                return of(null);
            }),
            finalize(() => {
                // this.ngxSpinnerService.hide();
                this.refreshResult();
            }),
        ).subscribe();
    }

    private getStorageKey(collection: string, field: string): string {
        return `${collection}-${field}-width`;
    }

    private getValue(name: string): number {
        return parseInt(localStorage.getItem(name), 10) || 0;
    }

    private setValue(name: string, value: number) {
        localStorage.setItem(name, value.toString());
    }

    private openForm(docData: { [key: string]: any }) {
        this.matDialog.open(FormComponent, {
            width: '75%',
            data: {
                docData,
                collection: this.collection
            }
        })
            .afterClosed()
            .subscribe((result) => {
                if (result) {
                    this.refreshResult();
                }
            });
    }

    private convertValue(data) {
        Object.keys(data).map(key => {
            if (data[key] === 'true') {
                data[key] = true;
            } else if (data[key] === 'false') {
                data[key] = false;
            } else if (isNaN(data[key]) === false) {
                data[key] = Number(data[key]);
            }
        });
        data['updated_at'] = firestore.FieldValue.serverTimestamp();
        return data;
    }

    onSelectBreadcrumb(bcItem) {
        if (bcItem.path !== null) {
            if (bcItem.queryParams) {
                const navigationExtras: NavigationExtras = {
                    queryParams: bcItem.queryParams
                };
                this.router.navigate(['/collection', bcItem.path], navigationExtras);
            } else {
                this.router.navigate(['/collection', bcItem.path]);
            }
        }
    }

    refreshResult() {
        // this.ngxSpinnerService.show();
        setTimeout(() => {
            this.ngxSpinnerService.hide();
            location.reload();
        }, 3000);
    }

    async onRowValueChanged(event: RowValueChangedEvent) {
        const data = event.data;
        this.ngxSpinnerService.show();
        const docSnap = await this.angularFirestore.collection(this.collection.id).doc(data.objectID).ref.get();
        const fbData = docSnap.data();
        this.collection.fields.forEach((field) => {
            switch (field.type) {
                case 'string':
                    fbData[field.id] = data[field.id];
                    break;
                case 'boolean':
                    fbData[field.id] = ('' + data[field.id]) === 'true';
                    break;
                case 'number':
                    if (data[field.id] !== '') { fbData[field.id] = Number(data[field.id]); } else { fbData[field.id] = NaN; }
                    break;
                case 'timestamp':
                    fbData[field.id] = firestore.FieldValue.serverTimestamp();
                    break;
                default:
                    break;
            }
        });
        from(this.angularFirestore.collection(this.collection.id).doc(docSnap.id).update(fbData))
            .pipe(
                catchError((error) => {
                    alert(error.message);
                    return of(null);
                }),
                finalize(() => {
                    // this.ngxSpinnerService.hide();
                    this.refreshResult();
                })
            )
            .subscribe();
    }
}
