import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

import { ConfigService } from 'src/core/service/config.service';

@Component({
    templateUrl: './descendant-cell-renderer.component.html'
})
export class DescendantCellRendererComponent implements ICellRendererAngularComp {

    params: ICellRendererParams;

    constructor(private router: Router, private configService: ConfigService) { }

    agInit(params: any) {
        this.params = params;
    }

    refresh(): boolean {
        return false;
    }

    onClick() {
        const descendant = (<any>this.params.colDef).descendant,
            collectionId = descendant.collection;

        this.router.navigate(['/collection', this.configService.routes.find((route) => route.collection === collectionId).path], {
            queryParamsHandling: 'merge',
            queryParams: {
                [descendant.field]: this.params.data.objectID
            }
        });
    }
}
