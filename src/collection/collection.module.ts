import { NgModule } from '@angular/core';
import { AgGridModule } from 'ag-grid-angular';

import { SharedModule } from 'src/shared/shared.module';
import { CollectionRoutingModule } from './collection-routing.module';

import { CollectionComponent } from './collection.component';
import { DescendantCellRendererComponent } from './descendant-cell-renderer/descendant-cell-renderer.component';
import { FormComponent } from './form/form.component';
import { ListPickerComponent } from './list-picker/list-picker.component';
import { CollectionGuard } from './guard/collection.guard';
import { NgAisModule } from 'angular-instantsearch';

@NgModule({
    declarations: [
        CollectionComponent,
        DescendantCellRendererComponent,
        FormComponent,
        ListPickerComponent
    ],
    entryComponents: [
        FormComponent,
        ListPickerComponent
    ],
    imports: [
        SharedModule,
        AgGridModule.withComponents([DescendantCellRendererComponent]),
        NgAisModule.forRoot(),
        CollectionRoutingModule
    ],
    providers: [
        CollectionGuard
    ]
})
export class CollectionModule { }
