import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, AbstractControl, FormControl, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { firestore } from 'firebase/app';
import { NgxSpinnerService } from 'ngx-spinner';
import { from, of } from 'rxjs';
import { catchError, tap, finalize } from 'rxjs/operators';
import * as moment from 'moment';
import * as pluralize from 'pluralize';

import { parseNumber, isNumber, valueToPercentage, percentageToValue, withInRange } from 'src/util/channel-view.util';
import { Collection, Option, Field, ConfigService, } from 'src/core/service/config.service';
import { ListPickerComponent } from '../list-picker/list-picker.component';

interface MatDialogData {
    collection: Collection;
    docData: { [key: string]: any };
}

const DefaultPrecision: number = 2, Bit8Min: number = 0, Bit8Max: number = 255, Bit8Length: number = 256;

@Component({
    templateUrl: './form.component.html',
    styleUrls: [
        './form.component.scss'
    ]
})
export class FormComponent {

    options: { [key: string]: any[] } = {};

    title: string;
    submitText: string;
    formGroup: FormGroup;
    dataLength: number;
    fields: Array<string> = [];
    activatedRoute: ActivatedRoute;

    constructor(activatedRoute: ActivatedRoute, private matDialog: MatDialog, private matDialogRef: MatDialogRef<FormComponent>,
        @Inject(MAT_DIALOG_DATA) public data: MatDialogData, private angularFirestore: AngularFirestore,
        private configService: ConfigService, private ngxSpinnerService: NgxSpinnerService, formBuilder: FormBuilder) {
        this.dataLength = Object.keys(data.docData).length;
        this.activatedRoute = activatedRoute;
        const queryParams = activatedRoute.snapshot.queryParams,
            collection = data.collection,
            docData = this.dataLength === 1 ? data.docData[0] : data.docData,
            isEdit = docData.hasOwnProperty('id');
        this.title = `${isEdit ? 'Edit' : 'Add'} ${pluralize.singular(configService.normalizeName(data.collection.id))}`;
        this.submitText = isEdit || this.dataLength > 1 ? 'Update' : 'Save';

        const controls: { [key: string]: AbstractControl } = {};
        collection.fields.forEach((field) => {
            const value = docData[field.id],
                validators = [];
            if (field.required) {
                validators.push(Validators.required);
            }
            if (field.pattern) {
                validators.push(Validators.pattern(field.pattern));
            }
            if (field.min !== void 0) {
                validators.push(Validators.min(field.min));
            }
            if (field.max !== void 0) {
                validators.push(Validators.max(field.max));
            }
            switch (field.type) {
                case 'string':
                case 'number':
                    let options = field.options;
                    if (options === true) {
                        options = require(`src/assets/data/${field.id}.json`);
                    }
                    this.options[field.id] = <Option[]>options;
                    break;
                case 'boolean':
                    if (value === void 0) {
                        docData[field.id] = false;
                    }
                    break;
                case 'reference':
                    const reference = field.reference,
                        refCollection = this.configService.collections.find((fCollection) => fCollection.id === reference),
                        refPrimaryField = refCollection.fields.find((rField) => rField.column);

                    if (value) {
                        docData[field.id] = value.id;
                    }

                    this.angularFirestore.collection(field.reference, (ref) => {
                        let query: firestore.CollectionReference | firestore.Query = ref;
                        for (const key in queryParams) {
                            if (queryParams[key]) {
                                const refField = refCollection.fields.find((rField) => rField.id === key);
                                if (refField) {
                                    query = query.where(refField.id, '==',
                                        this.angularFirestore.collection(refField.reference).doc(queryParams[key]).ref);
                                }
                            }
                        }

                        return query.orderBy(refPrimaryField.id, 'asc');
                    })
                        .get()
                        .subscribe((querySnapshot) => {
                            this.options[field.id] = querySnapshot.docs.map((tDoc) => {
                                const tDocData = tDoc.data();

                                let label = tDocData[refPrimaryField.id];
                                switch (refPrimaryField.cellRenderer) {
                                    case 'channelNumberCellRenderer':
                                        label = this.configService.normalizeChannelNumber(label);
                                        break;
                                }
                                if (tDocData['description']) {
                                    label = `${label} - ${tDocData['description']}`;
                                }

                                return {
                                    label,
                                    value: tDoc.id,
                                    data: tDocData
                                };
                            });
                        });
                    break;
                case 'timestamp':
                    if (value) {
                        docData[field.id] = moment(value.seconds, 'X').toDate();
                    }
                    break;
            }
            controls[field.id] = this.dataLength === 1 ? new FormControl(null, validators) : new FormControl(null);
        });
        this.formGroup = formBuilder.group(controls);

        if (!isEdit) {
            for (const key in queryParams) {
                if (queryParams[key] && collection.fields.find((field) => field.id === key)) {
                    docData[key] = queryParams[key];
                }
            }
        }

        this.formGroup.patchValue(docData);
    }

    onInputClick(field: Field) {
        if (field.options) {
            this.matDialog.open(ListPickerComponent, {
                width: '65%',
                data: {
                    title: `Click to select a ${field.name}`,
                    groups: this.options[field.id]
                }
            })
                .afterClosed()
                .subscribe((result) => {
                    if (result) {
                        this.formGroup.controls[field.id].patchValue(result);
                    }
                });
        }
    }

    onNumberInput(field: Field) {
        const collectionId = this.data.collection.id;
        let fixtureChannel, fieldVal, minVal, maxVal, userVal, bitVal;

        if (collectionId !== 'channel_functions') {
            switch (field.id) {
                case 'default_val':
                    fieldVal = parseNumber(this.formGroup.controls['default_val'].value);
                    minVal = parseNumber(this.formGroup.controls['user_min'].value);
                    maxVal = parseNumber(this.formGroup.controls['user_max'].value);

                    if (isNumber(fieldVal) && isNumber(minVal) && isNumber(maxVal)) {
                        userVal = valueToPercentage(fieldVal, minVal, maxVal);
                        bitVal = percentageToValue(userVal, Bit8Min, Bit8Length);

                        this.formGroup.controls['dmx_default_val'].setValue(
                            withInRange(Math.floor(bitVal), Bit8Min, Bit8Max),
                        );
                    }
                    break;
                case 'dmx_default_val':
                    fieldVal = parseNumber(this.formGroup.controls['dmx_default_val'].value);
                    minVal = parseNumber(this.formGroup.controls['user_min'].value);
                    maxVal = parseNumber(this.formGroup.controls['user_max'].value);

                    if (isNumber(fieldVal) && isNumber(minVal) && isNumber(maxVal)) {
                        bitVal = valueToPercentage(fieldVal, Bit8Min, Bit8Length);
                        userVal = percentageToValue(bitVal, minVal, maxVal);

                        this.formGroup.controls['default_val'].setValue(
                            +withInRange(userVal, minVal, maxVal).toFixed(DefaultPrecision),
                        );
                    }
                    break;
                case 'range_from':
                    if (this.options['channel_id']) {
                        fixtureChannel = this.options['channel_id'].find((option) =>
                            option.value === this.formGroup.controls['channel_id'].value);
                        fieldVal = parseNumber(this.formGroup.controls['range_from'].value);
                        minVal = parseNumber(fixtureChannel.data.user_min);
                        maxVal = parseNumber(fixtureChannel.data.user_max);

                        if (isNumber(fieldVal) && isNumber(minVal) && isNumber(maxVal)) {
                            userVal = valueToPercentage(fieldVal, minVal, maxVal);
                            bitVal = percentageToValue(userVal, Bit8Min, Bit8Length);

                            this.formGroup.controls['dmx_range_from'].setValue(
                                withInRange(Math.floor(bitVal), Bit8Min, Bit8Max),
                            );
                        }
                    }
                    break;
                case 'range_to':
                    if (this.options['channel_id']) {
                        fixtureChannel = this.options['channel_id'].find((option) =>
                            option.value === this.formGroup.controls['channel_id'].value);
                        fieldVal = parseNumber(this.formGroup.controls['range_to'].value);
                        minVal = parseNumber(fixtureChannel.data.user_min);
                        maxVal = parseNumber(fixtureChannel.data.user_max);

                        if (isNumber(fieldVal) && isNumber(minVal) && isNumber(maxVal)) {
                            userVal = valueToPercentage(fieldVal, minVal, maxVal);
                            bitVal = percentageToValue(userVal, Bit8Min, Bit8Length);

                            this.formGroup.controls['dmx_range_to'].setValue(
                                withInRange(Math.floor(bitVal), Bit8Min, Bit8Max),
                            );
                        }
                    }
                    break;
                case 'dmx_range_from':
                    if (this.options['channel_id']) {
                        fixtureChannel = this.options['channel_id'].find((option) =>
                            option.value === this.formGroup.controls['channel_id'].value);
                        fieldVal = parseNumber(this.formGroup.controls['dmx_range_from'].value);
                        minVal = parseNumber(fixtureChannel.data.user_min);
                        maxVal = parseNumber(fixtureChannel.data.user_max);

                        if (isNumber(fieldVal) && isNumber(minVal) && isNumber(maxVal)) {
                            bitVal = valueToPercentage(fieldVal, Bit8Min, Bit8Length);
                            userVal = percentageToValue(bitVal, minVal, maxVal);

                            this.formGroup.controls['range_from'].setValue(
                                +withInRange(userVal, minVal, maxVal).toFixed(DefaultPrecision),
                            );
                        }
                    }
                    break;
                case 'dmx_range_to':
                    if (this.options['channel_id']) {
                        fixtureChannel = this.options['channel_id'].find((option) =>
                            option.value === this.formGroup.controls['channel_id'].value);
                        fieldVal = parseNumber(this.formGroup.controls['dmx_range_to'].value);
                        minVal = parseNumber(fixtureChannel.data.user_min);
                        maxVal = parseNumber(fixtureChannel.data.user_max);

                        if (isNumber(fieldVal) && isNumber(minVal) && isNumber(maxVal)) {
                            bitVal = valueToPercentage(fieldVal, Bit8Min, Bit8Length);
                            userVal = percentageToValue(bitVal, minVal, maxVal);

                            this.formGroup.controls['range_to'].setValue(
                                +withInRange(userVal, minVal, maxVal).toFixed(DefaultPrecision),
                            );
                        }
                    }
                    break;
            }
        }
    }

    onCheckBoxHandler(event, field: Field) {
        if (event.checked && this.fields.indexOf(field.id) === -1) {
            this.fields.push(field.id);
        } else if (!event.checked && this.fields.indexOf(field.id) !== -1) {
            this.fields = this.fields.filter(e => e !== field.id);
        }
    }

    onUpdateButtonClick() {
        const collection = this.data.collection;
        let updateData = {};

        this.ngxSpinnerService.show();
        for (const key in this.data.docData) {
            if (this.data.docData[key] && !isNaN(Number(key))) {
                const docData = Object.assign({ 'deleted': false }, this.data.docData[key], this.formGroup.getRawValue());

                collection.fields.forEach((field) => {
                    switch (field.type) {
                        case 'reference':
                            docData[field.id] = docData[field.id] &&
                                this.angularFirestore.collection(field.reference).doc(docData[field.id]).ref || null;
                            break;
                        case 'timestamp':
                            docData[field.id] = docData[field.id] || firestore.FieldValue.serverTimestamp();
                            break;
                    }
                });

                const docId = docData.id || this.angularFirestore.createId();
                delete docData.id;

                if (this.dataLength > 1) {
                    this.fields.forEach(field => {
                        updateData[field] = docData[field];
                    });
                } else {
                    updateData = docData;
                }

                from(this.angularFirestore.collection(this.data.collection.id).doc(docId).update(updateData))
                    .pipe(
                        tap(() => {
                            this.matDialogRef.close(true);
                        }),
                        catchError((error) => {
                            alert(error.message);
                            return of(null);
                        }),
                        finalize(() => console.log('finalize'))
                    )
                    .subscribe();
            }
        }
    }

    onSaveButtonClick() {
        const collection = this.data.collection;
        this.ngxSpinnerService.show();
        const docData = Object.assign({ 'deleted': false }, this.data.docData, this.formGroup.getRawValue());

        collection.fields.forEach((field) => {
            switch (field.type) {
                case 'reference':
                    docData[field.id] = docData[field.id] &&
                        this.angularFirestore.collection(field.reference).doc(docData[field.id]).ref || null;
                    break;
                case 'timestamp':
                    docData[field.id] = docData[field.id] || firestore.FieldValue.serverTimestamp();
                    break;
            }
        });

        const docId = docData.id || this.angularFirestore.createId();
        delete docData.id;

        from(this.angularFirestore.collection(this.data.collection.id).doc(docId).set(docData))
            .pipe(
                tap(() => {
                    this.matDialogRef.close(true);
                }),
                catchError((error) => {
                    alert(error.message);
                    return of(null);
                }),
                finalize(() => console.log('finalize'))
            )
            .subscribe();
    }
}
